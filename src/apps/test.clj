(ns apps.test
  (:import (com.fazecast.jSerialComm SerialPort))
  (:require [link.frame]
            [link.mnp]
            [link.dock]
            [link.session]
            [clojure.tools.cli :refer [parse-opts]]
            [taoensso.timbre :as timbre :refer [info debug warn error]]))

(defn send-serial [data port]
  (debug "[serial] <<<" data)
  (comment ->> (data :down)
       (comment apply #(when-not (nil? %) (.writeBytes port % (count %)))))
  (when-let [msg (data :down)]
    (apply #(when-not (nil? %) (.writeBytes port % (count %))) msg)))

(defn handle-data [port data]
  (org.apache.commons.io.HexDump/dump data 0 System/out 0)
  (-> data
      (link.frame/receive)
      (link.mnp/receive)
      (link.dock/receive)
      (link.session/receive)
      (link.session/send)
      (link.dock/send)
      (link.mnp/send)
      (link.frame/send)
      (send-serial port)))

(defn read-data [port]
  (let [max-bytes 256
        buffer (byte-array max-bytes)
        bytes-read (.readBytes port buffer max-bytes)
        data (byte-array bytes-read)]
    (when (> bytes-read 0)
      (handle-data port (java.util.Arrays/copyOf buffer bytes-read)))
    (recur port)))

(defn get-port [name]
  (some
    #(and (or
            (clojure.string/includes? (.getSystemPortName %) name)
            (clojure.string/includes? (.getPortDescription %) name)) %) (SerialPort/getCommPorts)))

(defn connect [port-name speed]
  (info "Connecting to" port-name "at" speed "bps")
  (let [port (get-port port-name)]
    (.openPort port)
    (.setBaudRate port speed)
    (.setComPortTimeouts port SerialPort/TIMEOUT_READ_BLOCKING 100 0)
    (read-data port)))

(defn run [args]
  (timbre/set-level!
    (nth [:error :warn :info :debug] (args :verbosity)))
  (connect (args :port) (args :speed)))

(def OPTIONS
  [["-s" "--speed SPEED" "Serial speed" :default 115200]
   ["-p" "--port PORT" "Serial port" :default "/dev/ttyUSB0"]
   ["-v" nil "Log level" :id :verbosity :default 0 :update-fn inc]])

(defn -main
  [& args]
  (run (:options (parse-opts args OPTIONS))))
