(ns link.session
  (:require [tilakone.core :as tk :refer [_]]
            [link.dock-cmds :as cmd :refer
             [REQUEST_TO_DOCK NEWTON_NAME NEWTON_INFO RESULT SET_TIMEOUT PASSWORD DISCONNECT]]
            [taoensso.timbre :as timbre :refer [info debug warn error]]))

(def session-states
  [{::tk/name :idle
    ::tk/transitions [{::tk/on REQUEST_TO_DOCK  ::tk/to :initiate}
                      {::tk/on _                ::tk/to :idle}]}

   {::tk/name :initiate
    ::tk/transitions [{::tk/on NEWTON_NAME      ::tk/to :desktop-info}
                      {::tk/on REQUEST_TO_DOCK  ::tk/to :initiate}]}

   {::tk/name :desktop-info
    ::tk/transitions [{::tk/on NEWTON_INFO      ::tk/to :which-icons}]}

   {::tk/name :which-icons
    ::tk/transitions [{::tk/on RESULT           ::tk/to :set-timeout}]}

   {::tk/name :set-timeout
    ::tk/transitions [{::tk/on SET_TIMEOUT      ::tk/to :password}]}

   {::tk/name :password
    ::tk/transitions [{::tk/on PASSWORD         ::tk/to :up}]}

   {::tk/name :up
    ::tk/transitions [{::tk/on DISCONNECT       ::tk/to :idle}]}])

(def session-process
  {::tk/states session-states
   ::tk/state :idle
   ::tk/match? (fn [signal on] (= (signal :cmd) on))})

(def session-state (atom session-process))

(defn receive [data]
  (debug "[session] >>>" data)
  (when-not (empty? (data :up))
    (->> data :up
         (reduce tk/apply-signal @session-state)
         (reset! session-state)
         (prn)))
  (assoc data :up []))

(defn send [data]
  (debug "[session] <<<" data)
  data)
